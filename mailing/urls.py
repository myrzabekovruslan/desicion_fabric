from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import ClientViewSet, MailingViewSet, OperatorCodeViewSet, TagViewSet, MessageViewSet


router = DefaultRouter()
router.register('client', ClientViewSet, basename='client')
router.register('mailing', MailingViewSet, basename='mailing')
router.register('message', MessageViewSet, basename='message')
router.register('tag', TagViewSet, basename='tag')
router.register('operator_code', OperatorCodeViewSet, basename='operator-code')


urlpatterns = [
    path("", include(router.urls)),

]