from django.db import models
from django.core.validators import RegexValidator


class OperatorCode(models.Model):
    code = models.CharField(max_length=10, verbose_name='код мобильного оператора')

    def __str__(self):
        return f'{self.code}'

    class Meta:
        ordering = ['-id']
        verbose_name_plural = "Коды операторов"
        verbose_name = "Код оператора"


class Tag(models.Model):
    tag = models.CharField(max_length=25, verbose_name='тег (произвольная метка)')

    def __str__(self):
        return f'{self.tag}'

    class Meta:
        ordering = ['-id']
        verbose_name_plural = "Тэги клиентов"
        verbose_name = "Тэг"


class Client(models.Model):
    phone_number = models.IntegerField(
        unique=True,
        verbose_name='номер телефона клиента',
        validators=[
            RegexValidator(
                '^7\d{10}$',
                'Number format 7XXXXXXXXXX',
            )
        ]
    )
    operator_code = models.ForeignKey(
        OperatorCode,
        on_delete=models.CASCADE,
        related_name='clients',
        verbose_name='код мобильного оператора',
    )
    tag = models.ManyToManyField(
        Tag,
        related_name='clients',
        verbose_name='тег (произвольная метка)',
    )
    time_zone = models.CharField(max_length=10,
                                 null=True,
                                 blank=True,
                                 verbose_name='часовой пояс',
                                 validators=[
                                     RegexValidator(
                                         '^([+-](1|2|3|4|5|6|7|8|9|10|11|12)|0)$',
                                         '[-12,..,0,..,+12]',
                                     )
                                 ]
                                 )

    def __str__(self):
        return f'{self.phone_number}'

    class Meta:
        ordering = ['-id']
        verbose_name_plural = "Клиенты"
        verbose_name = "Клиент"


class Mailing(models.Model):
    created_date = models.DateTimeField(verbose_name='Дата и время запуска рассылки')
    text = models.CharField(max_length=500, verbose_name='Текст сообщения для доставки клиенту')
    end_date = models.DateTimeField(verbose_name='Дата и время окончания рассылки')
    operator_code = models.ManyToManyField(
        OperatorCode,
        blank=True,
        related_name='mailings',
        verbose_name='код мобильного оператора',
    )
    tag = models.ManyToManyField(
        Tag,
        blank=True,
        related_name='mailings',
        verbose_name='тег (произвольная метка)',
    )
    id_task = models.CharField(max_length=150, null=True, blank=True, verbose_name='ID задачи рассылки Celery')

    def __str__(self):
        return f'{self.id}, {self.created_date}'

    class Meta:
        ordering = ['-id']
        verbose_name_plural = "Рассылки"
        verbose_name = "Рассылка"


class Message(models.Model):
    client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
        related_name='messages',
        verbose_name='Клиент')
    mailing = models.ForeignKey(
        Mailing,
        on_delete=models.CASCADE,
        related_name='messages',
        verbose_name='Рассылка'
    )
    status = models.BooleanField(default=False, verbose_name='статус отправки')
    created_date = models.DateTimeField(auto_now_add=True, verbose_name='дата и время создания (отправки)')

    def __str__(self):
        return f'{self.id}, {self.client}'

    class Meta:
        ordering = ['-id']
        verbose_name_plural = "Сообщения"
        verbose_name = "Сообщение"
