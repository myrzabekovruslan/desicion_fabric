from rest_framework.test import APITestCase
from ..models import OperatorCode, Tag, Client, Message, Mailing


class OperatorCodeModelTests(APITestCase):

    def setUp(self):
        self.operator_code = OperatorCode.objects.create(code="705")

    def test_object_name_is_title_field(self):
        code = self.operator_code
        expected_object_name = "705"
        self.assertEquals(expected_object_name, str(code))


class TagModelTests(APITestCase):

    def setUp(self):
        self.tag = Tag.objects.create(tag="cars")

    def test_object_name_is_title_field(self):
        tag = self.tag
        expected_object_name = "cars"
        self.assertEquals(expected_object_name, str(tag))


class ClientModelTests(APITestCase):

    def setUp(self):
        self.operator_code = OperatorCode.objects.create(code="705")
        self.tag = Tag.objects.create(tag="cars")
        self.client = Client.objects.create(
            phone_number="77776665500",
            time_zone="+6",
            operator_code=self.operator_code,
        )
        self.client.tag.add(self.tag)

    def test_object_name_is_title_field(self):
        client = self.client
        expected_object_name = "77776665500"
        self.assertEquals(expected_object_name, str(client))


class MailingModelTests(APITestCase):

    def setUp(self):
        self.operator_code = OperatorCode.objects.create(code="705")
        self.tag = Tag.objects.create(tag="cars")
        self.mailing = Mailing.objects.create(
            created_date="2022-01-01 00:00:00",
            text="hello sailor moon",
            end_date="2022-01-01 01:00:00",
            id_task="9dawd-dwad-32fes-awd32e3",
        )
        self.mailing.operator_code.add(self.operator_code)
        self.mailing.tag.add(self.tag)

    def test_object_name_is_title_field(self):
        mailing = self.mailing
        expected_object_name = f"{self.mailing.id}, {self.mailing.created_date}"
        self.assertEquals(expected_object_name, str(mailing))


class MessageModelTests(APITestCase):

    def setUp(self):
        self.operator_code = OperatorCode.objects.create(code="705")
        self.tag = Tag.objects.create(tag="cars")
        self.client = Client.objects.create(
            phone_number="77776665500",
            time_zone="+6",
            operator_code=self.operator_code,
        )
        self.client.tag.add(self.tag)
        self.mailing = Mailing.objects.create(
            created_date="2022-01-01 00:00:00",
            text="hello sailor moon",
            end_date="2022-01-01 01:00:00",
            id_task="9dawd-dwad-32fes-awd32e3",
        )
        self.mailing.operator_code.add(self.operator_code)
        self.mailing.tag.add(self.tag)
        self.message = Message.objects.create(
            client=self.client,
            mailing=self.mailing,
            status=True,
        )

    def test_object_name_is_title_field(self):
        message = self.message
        expected_object_name = f"{self.message.id}, {self.client.phone_number}"
        self.assertEquals(expected_object_name, str(message))
