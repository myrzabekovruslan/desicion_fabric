from rest_framework import serializers
from .models import Mailing, Client, Message, OperatorCode, Tag


class OperatorCodeSerializer(serializers.ModelSerializer):
    """Сериализатор для кодов операторов"""

    class Meta:
        model = OperatorCode
        fields = "__all__"


class TagSerializer(serializers.ModelSerializer):
    """Сериализатор для тэгов"""

    class Meta:
        model = Tag
        fields = "__all__"


class MailingSerializer(serializers.ModelSerializer):
    """Сериализатор для рассылок"""

    class Meta:
        model = Mailing
        fields = "__all__"
        read_only_fields = ("id_task",)


class ClientSerializer(serializers.ModelSerializer):
    """Сериализатор для клиентов"""

    class Meta:
        model = Client
        fields = "__all__"


class MessageSerializer(serializers.ModelSerializer):
    """Сериализатор для сообщений"""

    class Meta:
        model = Message
        fields = "__all__"


class MailingStaticticsSerializer(serializers.ModelSerializer):
    """Сериализатор для статистики рассылок"""
    messages = MessageSerializer(many=True)

    class Meta:
        model = Mailing
        fields = ("id", "messages")
