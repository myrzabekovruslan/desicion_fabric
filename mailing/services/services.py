from ..models import Message, Mailing, Client
from django.db.models import Q


def create_mailing_messages(id: int) -> bool:
    try:
        mailing = Mailing.objects.get(id=id)
        clients = Client.objects.filter(
            Q(operator_code__in=mailing.operator_code.all()) |
            Q(tag__in=mailing.tag.all())
        ).distinct()
        for client in clients:
            message = Message(client=client, mailing=mailing)
            message.save()
        return True
    except Exception as err:
        print(err)
        return False


def delete_old_messages(id: int) -> bool:
    try:
        mailing = Mailing.objects.get(id=id)
        Message.objects.filter(mailing=mailing).delete()
        return True
    except Exception as err:
        print(err)
        return False