from rest_framework import viewsets, permissions, generics, status, serializers, mixins
from .serializers import MailingSerializer, ClientSerializer, MessageSerializer, OperatorCodeSerializer, TagSerializer, \
    MailingStaticticsSerializer
from .models import Client, Message, Mailing, OperatorCode, Tag
from rest_framework.response import Response
from rest_framework.decorators import action
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from .tasks import send_mailing
from datetime import datetime
from .services.services import create_mailing_messages, delete_old_messages
from django.utils.dateparse import parse_datetime
from django.utils import timezone
from decision_fabric.celery import app


class OperatorCodeViewSet(viewsets.ModelViewSet):
    """Вьюшки для операторских кодов"""
    queryset = OperatorCode.objects.all()
    serializer_class = OperatorCodeSerializer


class TagViewSet(viewsets.ModelViewSet):
    """Вьюшки для тэгов"""
    queryset = Tag.objects.all()
    serializer_class = TagSerializer


class MailingViewSet(viewsets.ModelViewSet):
    """Вьюшки для рассылок"""
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        # create all message records with status false
        if not create_mailing_messages(id=serializer.data.get('id')):
            return Response(serializer.data, status=status.HTTP_503_SERVICE_UNAVAILABLE)

        task = None

        try:
            created_date = parse_datetime(serializer.data.get('created_date'))
            end_date = parse_datetime(serializer.data.get('end_date'))

            if created_date <= timezone.now() < end_date:
                task = send_mailing.delay(serializer.data.get('id'))
            elif timezone.now() < created_date < end_date:
                task = send_mailing.apply_async((serializer.data.get('id'),), eta=created_date)

        except Exception as err:
            print(err)
            return Response(serializer.data, status=status.HTTP_503_SERVICE_UNAVAILABLE)

        if task:
            instance = self.queryset.get(id=serializer.data.get('id'))
            serializer = self.get_serializer(instance, data={'id_task': task.id}, partial=True)
            serializer.is_valid(raise_exception=True)
            serializer.save(id_task=task.id)

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if not delete_old_messages(id=serializer.data.get('id')):
            return Response(serializer.data, status=status.HTTP_503_SERVICE_UNAVAILABLE)

        if not create_mailing_messages(id=serializer.data.get('id')):
            return Response(serializer.data, status=status.HTTP_503_SERVICE_UNAVAILABLE)

        task = None

        try:
            created_date = parse_datetime(serializer.data.get('created_date'))
            end_date = parse_datetime(serializer.data.get('end_date'))

            if created_date <= timezone.now() < end_date:
                app.control.revoke(serializer.data.get('id_task'), terminate=True)
                task = send_mailing.delay(serializer.data.get('id'))
            elif timezone.now() < created_date < end_date:
                app.control.revoke(serializer.data.get('id_task'), terminate=True)
                task = send_mailing.apply_async((serializer.data.get('id'),), eta=created_date)

        except Exception as err:
            print(err)
            return Response(serializer.data, status=status.HTTP_503_SERVICE_UNAVAILABLE)

        if task:
            instance = self.queryset.get(id=serializer.data.get('id'))
            serializer = self.get_serializer(instance, data={'id_task': task.id}, partial=True)
            serializer.is_valid(raise_exception=True)
            serializer.save(id_task=task.id)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        try:
            if timezone.now() < instance.end_date:
                app.control.revoke(instance.id_task, terminate=True)
        except Exception as err:
            print(err)
            return Response(status=status.HTTP_503_SERVICE_UNAVAILABLE)
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    @swagger_auto_schema(
        methods=['GET', ],
        operation_description='Получение статистики по всем рассылкам',
        responses={
            200: MailingStaticticsSerializer(many=True),
        }
    )
    @action(detail=False, url_path='statictics', url_name='get-mailing-statistics', methods=['GET'])
    def allStatictics(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        serializer = MailingStaticticsSerializer(queryset, many=True)
        # Message.
        return Response(serializer.data)

    @swagger_auto_schema(
        methods=['GET', ],
        operation_description='Получение статистики для одной рассылки',
        manual_parameters=[
            openapi.Parameter(
                'id',
                in_=openapi.IN_PATH,
                description='id рассылки',
                type=openapi.TYPE_INTEGER,
                required=True,
            ),
        ],
        responses={
            200: MailingStaticticsSerializer,
        }
    )
    @action(detail=True, url_path='statictics', url_name='get-detail-mailing-statistics', methods=['GET'])
    def detailStatistics(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = MailingStaticticsSerializer(instance)
        return Response(serializer.data)


class ClientViewSet(viewsets.ModelViewSet):
    """Вьюшки для клиентов"""
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class MessageViewSet(viewsets.ModelViewSet):
    """Вьюшки для сообщений"""
    queryset = Message.objects.all()
    serializer_class = MessageSerializer