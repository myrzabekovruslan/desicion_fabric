from __future__ import absolute_import
from celery import shared_task
import requests
import os
from django.conf import settings
from .models import Mailing, Client, Message
from django.db.models import Q
import requests
from rest_framework import status
from django.utils import timezone


@shared_task(bind=True, max_retries=None)
def send_mailing(self, id: int) -> None:
    try:
        mailing = Mailing.objects.get(id=id)
        if mailing.end_date < timezone.now():
            return
        url = "https://probe.fbrq.cloud/v1/send/"
        clients = Client.objects.filter(
            messages__mailing=mailing,
            messages__status=False
        )
        for client in clients:
            try:
                message = Message.objects.get(client=client, mailing=mailing)
                response = requests.post(
                    f'{url}{message.id}',
                    headers={
                        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2ODA2MTQxMzQsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6IlJ1c2xhbktLa2V5In0.Og7QnoZYhlPf3BDxhed2bt6qjw_xdgdHa2pNMQxDeTE'
                    },
                    data={
                        'id': message.id,
                        'phone': client.phone_number,
                        'text': mailing.text,
                    }
                )
                if response.status_code == status.HTTP_200_OK:
                    message.status = True
                    message.save()
                else:
                    raise Exception({"error": response.text})
            except Exception as err:
                raise err
    except Mailing.DoesNotExist:
        return
    except Exception as err:
        raise self.retry(exc=err, countdown=60)